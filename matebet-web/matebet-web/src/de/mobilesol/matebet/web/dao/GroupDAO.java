package de.mobilesol.matebet.web.dao;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import de.mobilesol.matebet.web.dto.GroupDTO;

public class GroupDAO {
	private static final Logger log = Logger
			.getLogger(GroupDAO.class.getName());
	private static final String TABLE = "group_t";

	public int addGroup(GroupDTO a) {
		log.info("addGroup" + a);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, a.groupId);
		try {
			datastore.get(key);
			throw new IllegalStateException(
					"id " + a.groupId + " already exists,");
		} catch (EntityNotFoundException e1) {
			Entity e = toEntity(new Entity(key), a);
			e.setProperty("created", new Date());

			datastore.put(e);

			return a.groupId;
		}
	}

	public void updateGroup(GroupDTO a) throws EntityNotFoundException {
		log.info("updateGroup" + a);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, a.groupId);
		Entity e = toEntity(datastore.get(key), a);
		e.setProperty("modified", new Date());
		datastore.put(e);
	}

	public GroupDTO getGroup(int id) {

		GroupDTO g = new GroupDTO();
		g.name = "EOD";
		g.groupId = 1;
		g.groupOrderId = 1;
		return g;
	}

	public List<GroupDTO> getAllGroups(int leagueId) {

		List<GroupDTO> groups = new ArrayList();
		GroupDTO g1 = new GroupDTO();
		g1.name = "EndOfDay";
		g1.groupId = 100;
		g1.firstMatchdate = new Date();
		
		Calendar c = new GregorianCalendar();
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		g1.lastMatchdate = c.getTime();
		groups.add(g1);
		return groups;
	}

	public List<GroupDTO> deleteGroupsByLeague(int leagueId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("leagueId", FilterOperator.EQUAL,
				leagueId));

		PreparedQuery pq = datastore.prepare(q);

		List<GroupDTO> list = new ArrayList<GroupDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			GroupDTO b = toObject(e);
			datastore.delete(e.getKey());
			list.add(b);
		}

		return list;
	}

	private Entity toEntity(Entity e, GroupDTO a) {
		e.setProperty("groupId", a.groupId);
		e.setProperty("leagueId", a.leagueId);
		e.setProperty("groupOrderId", a.groupOrderId);
		e.setProperty("name", a.name);
		e.setProperty("season", a.season);
		e.setProperty("lastMatchdate", a.lastMatchdate.getTime());
		e.setProperty("firstMatchdate", a.firstMatchdate.getTime());

		return e;
	}

	private GroupDTO toObject(Entity e) {
		GroupDTO a = new GroupDTO();
		a.name = (String) e.getProperty("name");
		if (a.name == null || a.name.trim().length() == 0) {
			// workaround as group name maybe null
			a.name = "-";
		}
		a.season = (String) e.getProperty("season");
		a.groupId = ((Number) e.getProperty("groupId")).intValue();
		a.leagueId = ((Number) e.getProperty("leagueId")).intValue();
		a.groupOrderId = ((Number) e.getProperty("groupOrderId")).intValue();
		a.lastMatchdate = new Date(
				((Number) e.getProperty("lastMatchdate")).longValue());
		a.firstMatchdate = new Date(
				((Number) e.getProperty("firstMatchdate")).longValue());

		return a;
	}
}
