package de.mobilesol.matebet.web.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import de.mobilesol.matebet.web.dto.LeagueDTO;

public class LeagueDAO {
	private static final Logger log = Logger
			.getLogger(LeagueDAO.class.getName());

	private List<LeagueDTO> leagues = new ArrayList<LeagueDTO>();
	public LeagueDAO() {
		LeagueDTO l = new LeagueDTO();
		l.title="DAX";
		l.leagueId=1;
		
		leagues.add(l);
		
		l = new LeagueDTO();
		l.title="WIG20";
		l.leagueId=2;

		leagues.add(l);
		
	}
	public LeagueDTO addLeague(LeagueDTO a) {
		log.info("addLeague" + a);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey("league_t", a.leagueId);
		Entity e = new Entity(key);
		e.setProperty("created", new Date());
		e.setProperty("id", a.leagueId);
		e.setProperty("title", a.title);
		e.setProperty("saison", a.saison);
		e.setProperty("shortName", a.shortName);

		datastore.put(e);

		return a;
	}

	public List<LeagueDTO> getAllLeagues(boolean incDisabled) {

		return leagues;
	}

	public LeagueDTO getLeague(int id) {
		LeagueDTO l = new LeagueDTO();
		l.leagueId = 1;
		l.title = "DAX";
		return l;
	}

	public LeagueDTO deleteLeague(int id) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query("league_t");
		q.setFilter(new FilterPredicate("id", FilterOperator.EQUAL, id));

		PreparedQuery pq = datastore.prepare(q);
		Iterator<Entity> it = pq.asIterator();
		if (it.hasNext()) {
			Entity e = it.next();
			LeagueDTO a = toObject(e);
			datastore.delete(e.getKey());
			return a;
		}

		return null;
	}

	public LeagueDTO disableLeague(int id, boolean disable) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query("league_t");
		q.setFilter(new FilterPredicate("id", FilterOperator.EQUAL, id));

		PreparedQuery pq = datastore.prepare(q);
		Iterator<Entity> it = pq.asIterator();
		if (it.hasNext()) {
			Entity e = it.next();
			if (disable) {
				e.setProperty("disabled", disable);
			} else {
				e.removeProperty("disabled");
			}
			datastore.put(e);

			return toObject(e);
		}

		return null;
	}

	protected LeagueDTO toObject(Entity e) {
		LeagueDTO a = new LeagueDTO();
		a.leagueId = ((Number) e.getProperty("id")).intValue();
		a.title = (String) e.getProperty("title");
		a.saison = (String) e.getProperty("saison");
		a.shortName = (String) e.getProperty("shortName");

		Boolean b = (Boolean) e.getProperty("disabled");
		if (b != null && b) {
			a.disabled = true;
		}
		return a;
	}
}
