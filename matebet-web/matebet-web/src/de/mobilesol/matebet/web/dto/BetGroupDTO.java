package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class BetGroupDTO implements Serializable {

	private static final long serialVersionUID = -2453704448083450895L;

	public List<BetMatchDTO> matches;
	public int groupId;
	public String name;

	public Date firstMatchdate;
	public Date lastMatchdate;

	@Override
	public String toString() {
		return "BetGroupDTO [matches=" + matches + ", groupId=" + groupId
				+ ", name=" + name + "]";
	}
}
