package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.Date;

public class MatchDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public int matchId;
	public int leagueId;
	public String season;
	public int groupId, groupOrderId;
	public Date matchdate;
	public TeamDTO team1, team2;
	public boolean finished;
	public Integer score1, score2;

	public Date lastUpdate;
	public String txt;

	public MatchDTO() {
		team1 = new TeamDTO();
		team2 = new TeamDTO();
	}

	@Override
	public String toString() {
		return "MatchDTO [id=" + matchId + ", matchdate=" + matchdate
				+ ", team1=" + team1 + ", team2=" + team2
				+ (score1 != null
						? ", score1=" + score1 + ", score2=" + score2
						: "")
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + matchId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchDTO other = (MatchDTO) obj;
		if (matchId != other.matchId)
			return false;
		return true;
	}

}
