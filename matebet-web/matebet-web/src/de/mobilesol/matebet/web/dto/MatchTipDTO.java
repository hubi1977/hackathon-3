package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.Map;

public class MatchTipDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public int betId;

	public Map<Integer, TipsByMatchByGroupMap> groupsByUser;

	public static class TipsByMatchByGroupMap implements Serializable {

		private static final long serialVersionUID = 1L;

		// tips by group
		public Map<Integer, TipsByMatchMap> groups;

		@Override
		public String toString() {
			return "TipsByMatchByGroupMap [groups=" + groups + "]";
		}
	}

	public static class TipsByMatchMap implements Serializable {
		private static final long serialVersionUID = 1L;

		// tips by match
		public Map<Integer, TipDTO> tips;

		@Override
		public String toString() {
			return "TipsByMatchMap [tips=" + tips + "]";
		}
	}

	public static class TipDTO implements Serializable {
		private static final long serialVersionUID = 1L;

		public int tipTeam1, tipTeam2;

		@Override
		public String toString() {
			return "TipDTO [" + tipTeam1 + ":" + tipTeam2 + "]";
		}
	}

	@Override
	public String toString() {
		return "MatchTipDTO [betId=" + betId + ", groupsByUser=" + groupsByUser
				+ "]";
	}
}
