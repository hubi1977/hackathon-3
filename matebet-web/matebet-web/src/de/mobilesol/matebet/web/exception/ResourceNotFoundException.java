package de.mobilesol.matebet.web.exception;

public class ResourceNotFoundException extends MatebetException {

	private static final long serialVersionUID = 1L;

	public ResourceNotFoundException() {
		super();
	}

	public ResourceNotFoundException(int code, String message) {
		super(code, message);
	}
}
