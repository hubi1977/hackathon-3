package de.mobilesol.matebet.web.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dao.FriendDAO;
import de.mobilesol.matebet.web.dto.FriendDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO.Page;
import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.dto.contact.ContactDTO;
import de.mobilesol.matebet.web.dto.contact.ContactDTO.ContactEmail;
import de.mobilesol.matebet.web.exception.MatebetException;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;

public class FriendService {
	private static final Logger log = Logger
			.getLogger(FriendService.class.getName());

	private FriendDAO dao = new FriendDAO();

	private FriendRequestService friendRequestService = new FriendRequestService();
	private ContactService contactService = new ContactService();
	private UserService userService = new UserService();
	private PushService pushService = new PushService();

	public List<FriendDTO> getFriends(int id) {
		log.info("getFriends(" + id + ")");

		List<FriendDTO> friends = new ArrayList<FriendDTO>();

		// and now add your friends
		List<Integer> users2Ids = dao.getFriends(id);
		List<FriendDTO> users2 = new ArrayList<FriendDTO>();
		for (int n : users2Ids) {
			FriendDTO u = getFriend(n);
			if (u == null) {
				log.warn("user " + n + " does not exist.");
				continue;
			}
			if (u.picture == null) {
				u.picture = "http://api.matebet.net/images/photo.jpg";
			}
			friends.add(u);
		}

		friends.addAll(users2);

		Collections.sort(friends, comparator);

		// add friend requests first
		List<Integer> users = friendRequestService.getFriendRequests(id);
		for (int userId : users) {
			UserDTO u = userService.getUser(userId, false);
			if (u == null) {
				continue;
			}
			FriendDTO f = new FriendDTO();
			f.request = true;
			f.friendId = u.userId;
			f.name = u.name;
			f.picture = u.picture;
			friends.add(0, f);
		}
		
		FriendDTO f = new FriendDTO();
		f.name="<public>";
		f.friendId = 1;
		friends.add(f);

		return friends;
	}

	public FriendDTO getFriend(int id) {
		UserDTO u = userService.getUser(id, false);
		if (u == null) {
			return null;
		}
		FriendDTO a = new FriendDTO();
		a.name = u.name;
		a.picture = u.picture;
		a.friendId = u.userId;

		return a;
	}

	public List<FriendDTO> getFriend(int userId, String query) {
		log.info("getFriend(" + userId + ", " + query + ")");
		UserDTO user = userService.getUserByName(query);
		if (user == null) {
			throw new ResourceNotFoundException(
					MatebetException.EXC_USER_NOT_FOUND,
					"Did not find a user " + query);
		}

		Set<Integer> usersThatAreAlreadyFriends = new HashSet<Integer>();
		List<FriendDTO> friends = getFriends(userId);
		log.info("friends=" + friends);
		for (FriendDTO f : friends) {
			usersThatAreAlreadyFriends.add(f.friendId);
		}

		// already requested friends
		Set<Integer> friendRequests = friendRequestService
				.getFriendRequestsByUser(userId);

		List<FriendDTO> ret = new ArrayList<FriendDTO>();

		// remove yourself
		if (user.userId == userId) {
			return ret;
		}

		FriendDTO friend = new FriendDTO();
		friend.friendId = user.userId;
		friend.name = user.name;
		friend.picture = user.picture;
		friend.isFriend = usersThatAreAlreadyFriends.contains(user.userId);

		// has already been requested
		if (friendRequests.contains(user.userId)) {
			log.info("friend has already been requested");
			friend.alreadyRequested = true;
		}

		ret.add(friend);

		return ret;
	}

	public UserDTO sendInvitationRequest(int userId, int friendId) {
		log.info("sendInvitationRequest(" + userId + ", " + friendId + ")");

		UserDTO me = userService.getUser(userId, false);

		friendRequestService.addFriendRequest(userId, friendId);

		pushService.send(friendId,
				new PushNotificationDTO(
						me.name + " hat dir eine mateBet-Anfrage geschickt.",
						"Kennst du " + me.name
								+ " und willst ihn in deine Freundesliste aufnehmen?",
						Page.FRIENDS, null));

		return userService.getUser(friendId, false);
	}

	public List<FriendDTO> acceptInvitationRequest(int userId,
			FriendDTO friend) {
		log.info("acceptInvitationRequest(" + userId + ", " + friend + ")");

		addFriend(userId, friend.friendId, true);
		addFriend(friend.friendId, userId, true);

		UserDTO user = userService.getUser(userId, false);

		pushService.send(friend.friendId,
				new PushNotificationDTO(
						user.name + " hat deine Anfrage bestätigt.",
						user.name
								+ " hat deine Freundschaftsanfrage bestätigt. Fordere ihn jetzt heraus!",
						Page.FRIENDS, null));

		return getFriends(userId);
	}

	public List<FriendDTO> rejectInvitationRequest(int userId,
			FriendDTO friend) {
		log.info("rejectInvitationRequest(" + userId + ", " + friend + ")");

		friendRequestService.removeFriendRequest(userId, friend.friendId);
		friendRequestService.removeFriendRequest(friend.friendId, userId);

		UserDTO user = userService.getUser(userId, false);

		pushService.send(friend.friendId,
				new PushNotificationDTO(
						user.name + " hat deine Anfrage abgelehnt.",
						user.name
								+ " hat deine Freundschaftsanfrage abgelehnt.",
						Page.FRIENDS, null));

		return getFriends(userId);
	}

	public boolean addFriend(int userId, int friendId, boolean forceAdd) {
		log.info("addFriend(" + userId + ", " + friendId + ")");
		friendRequestService.removeFriendRequest(userId, friendId);
		if (userId != friendId) {
			return dao.addFriend(userId, friendId, forceAdd);
		} else {
			log.warn("do not add myself as friend");
			return false;
		}
	}

	public List<FriendDTO> removeFriend(int userId, int friendId) {
		dao.removeFriend(userId, friendId);

		// explicityly requested by peter
		dao.removeFriend(friendId, userId);

		List<FriendDTO> friends = getFriends(userId);

		// workaround for gae
		List<FriendDTO> fs = new ArrayList<FriendDTO>();
		for (FriendDTO f : friends) {
			if (f.friendId != friendId) {
				fs.add(f);
			}
		}

		return fs;
	}

	public boolean isDeleted(int userId, int friendId) {
		return dao.isDeleted(userId, friendId);
	}

	public List<FriendDTO> updateContacts(int userId,
			List<ContactDTO> contacts) {

		log.info("updateContacts(" + userId + ") #=" + contacts.size());
		contactService.updateContacts(userId, contacts);

		// contacts
		Map<String, ContactDTO> contactByEmail = new HashMap<String, ContactDTO>();
		for (ContactDTO c : contacts) {
			if (c.emails != null) {
				for (ContactEmail e : c.emails) {
					contactByEmail.put(e.value, c);
				}
			}
		}

		List<FriendDTO> users = new ArrayList<FriendDTO>();

		// add current friends to users
		List<FriendDTO> currentFriends = getFriends(userId);
		Set<Integer> currentFriendSet = new HashSet<Integer>();
		for (FriendDTO f : currentFriends) {
			currentFriendSet.add(f.friendId);
			users.add(f);
		}

		log.info("updateContacts map=" + contactByEmail.size());

		// add non-friends to users
		List<UserDTO> knownUsers = userService
				.getUsers(contactByEmail.keySet());
		Set<String> knownEmails = new HashSet<String>();
		for (UserDTO u : knownUsers) {
			knownEmails.add(u.email);

			if (userId != u.userId && !currentFriendSet.contains(u.userId)) {
				FriendDTO f = new FriendDTO();
				f.friendId = u.userId;
				f.name = u.name;
				f.picture = u.picture;

				boolean b = addFriend(userId, u.userId, false);
				addFriend(u.userId, userId, false);
				if (b) {
					users.add(f);
				}
				currentFriendSet.add(u.userId);
			}
		}

		// now add contacts that are not known
		for (ContactDTO c : contacts) {
			// log.info("contact " + c);
			if (c.displayName == null) {
				continue;
			}

			boolean found = false;
			boolean emailExists = false;
			String email = null;
			if (c.emails != null) {
				for (ContactEmail e : c.emails) {
					if (e.value != null && e.value.length() > 1) {
						emailExists = true;
						email = e.value;
						if (knownEmails.contains(e.value)) {
							found = true;
							break;
						}
					}
				}
			}

			if (!found && emailExists) {

				FriendDTO f = new FriendDTO();
				f.friendId = c.id;
				f.name = c.displayName;
				f.email = email;

				// users.add(f);
			}
		}

		log.info("returning #users=" + users.size());
		Collections.sort(users, comparator);

		return users;
	}

	private Comparator<FriendDTO> comparator = new Comparator<FriendDTO>() {

		@Override
		public int compare(FriendDTO o1, FriendDTO o2) {
			int ret = o1.name.compareToIgnoreCase(o2.name);
			if (ret != 0) {
				return ret;
			}
			return o1.friendId - o2.friendId;
		}

	};
}
