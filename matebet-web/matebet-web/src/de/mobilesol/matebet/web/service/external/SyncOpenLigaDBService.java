package de.mobilesol.matebet.web.service.external;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.soap.SOAPFaultException;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.mobilesol.matebet.web.dto.GroupDTO;
import de.mobilesol.matebet.web.dto.LeagueDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.dto.SyncOpenLigaDBResultDTO;
import de.mobilesol.matebet.web.dto.TeamDTO;
import de.mobilesol.matebet.web.service.master.GroupService;
import de.mobilesol.matebet.web.service.master.LeagueService;
import de.mobilesol.matebet.web.service.master.MatchService;
import de.mobilesol.matebet.web.service.master.TeamService;
import de.mobilesol.matebet.web.util.SendMail;
import de.mobilesol.matebet.web.util.Util;

public class SyncOpenLigaDBService {

	private static final Logger log = Logger
			.getLogger(SyncOpenLigaDBService.class.getName());

	private OpenLigaDBService openLigaService = new OpenLigaDBService();
	private LeagueService leagueService = new LeagueService();
	private GroupService groupService = new GroupService();
	private MatchService matchService = new MatchService();
	private TeamService teamService = new TeamService();

	private Gson gson = new GsonBuilder().setPrettyPrinting()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ").create();

	public void importLeague(int leagueId) {

		log.info("importing league=" + leagueId);

		// get league
		List<LeagueDTO> leagueList = openLigaService.getLeagues();
		LeagueDTO l = null;

		for (LeagueDTO leagueDTO : leagueList) {
			if (leagueDTO.leagueId == leagueId) {
				l = leagueDTO;
				break;
			}
		}

		if (l == null) {
			return;
		}
		log.info("League to import: " + l);

		//
		// read teams
		//
		List<TeamDTO> allTeams = openLigaService.getTeams(l.leagueId);

		//
		// read groups
		//
		List<GroupDTO> allGroups = openLigaService.getGroups(l.leagueId);

		log.info("allTeams=" + allTeams.size());
		log.info("allGroups=" + allGroups.size());

		leagueService.addLeague(l);

		for (TeamDTO t : allTeams) {
			try {
				teamService.addTeam(t);
			} catch (IllegalStateException e) {

			}
		}

		for (GroupDTO t : allGroups) {

			//
			// all matches per group
			//
			List<MatchDTO> allMatches = openLigaService.getMatches(l.leagueId,
					t.groupId);

			log.info("allMatches=" + allMatches.size());

			for (MatchDTO m : allMatches) {

				try {
					matchService.addMatch(m);
				} catch (IllegalStateException e) {
					// ok it already exists
				}
			}

			try {
				groupService.addGroup(t);
			} catch (IllegalStateException e) {
				// ok if it already exists
			}
		}

		log.info("wrote teams: " + allTeams + "\n\n");
		log.info("wrote groups: " + allGroups + "\n\n");
	}

	public List<MatchDTO> syncLatestMatches(int days) {
		log.info("syncLatestMatches(" + days + ")");
		List<MatchDTO> matches = matchService.getLatestMatches(days);

		log.info("found " + matches.size() + " matches");
		List<MatchDTO> res = new ArrayList<MatchDTO>();
		for (MatchDTO m : matches) {
			log.info("found Match: " + m + ";" + m.matchdate);

			try {
				MatchDTO mr = openLigaService.getMatchresult(m.matchId);
				if (mr != null) {
					if (mr.score1 >= 0 && mr.score2 >= 0) {
						log.info("updating Match: " + mr);
						MatchDTO mr1 = matchService.setMatchResult(mr);
						res.add(mr1);
					}
				}
			} catch (SOAPFaultException ex) {
				log.warn("soap fault", ex);
			}
		}

		if (res.size() > 0) {
			Gson gson = new GsonBuilder().setPrettyPrinting()
					.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ").create();

			for (MatchDTO m : res) {
				m.team1.name = teamService.getTeam(m.team1.teamId).name;
				m.team2.name = teamService.getTeam(m.team2.teamId).name;
			}

			new SendMail().send("SyncOpenLigaDBService",
					"<p>I found " + res.size() + " New match results</p>"
							+ "<pre>" + gson.toJson(res) + "</pre>",
					"lui", "lui.baeumer@gmail.com", null);
		}
		return res;
	}

	private LeagueDTO[] getLeagueDelta(LeagueDTO l1) {
		List<LeagueDTO> ls = openLigaService.getLeagues();
		for (LeagueDTO l : ls) {
			if (l.leagueId == l1.leagueId) {
				if (!equals(l.saison, l1.saison)
						|| !equals(l.title, l1.title)) {
					return new LeagueDTO[]{l1, l};
				}
			}
		}

		return null;
	}

	public SyncOpenLigaDBResultDTO fixLeague(int leagueId, boolean change) {
		log.info("fixLeague(" + leagueId + "," + change + ")");
		SyncOpenLigaDBResultDTO res = new SyncOpenLigaDBResultDTO();

		LeagueDTO l1 = leagueService.getLeague(leagueId);

		// check league
		res.leagueDelta = getLeagueDelta(l1);

		// check groups
		List<GroupDTO> groups1 = groupService.getAllGroups(leagueId, false);
		Map<Integer, GroupDTO> groupById = new HashMap<Integer, GroupDTO>();
		for (GroupDTO g : groups1) {
			groupById.put(g.groupId, g);
		}

		List<GroupDTO> groups2 = openLigaService.getGroups(leagueId);

		res.groupDelta = delta(groups1, groups2);

		for (int i = 0; i < groups2.size(); i++) {

			log.info("handle group " + groups2.get(i).name);

			// handle matches
			GroupDTO g1 = groupById.get(groups2.get(i).groupId);
			List<MatchDTO> matches1 = (g1 != null
					? matchService.getMatchesByGroup(g1.groupId)
					: Collections.EMPTY_LIST);
			List<MatchDTO> matches2 = openLigaService.getMatches(leagueId,
					groups2.get(i).groupId);

			List<MatchDTO[]> delta = deltaM(matches1, matches2);
			if (delta.size() > 0) {
				log.info("match delta=" + delta);
				res.matchDelta.addAll(delta);
			}
		}

		res.groupDeltaCount = res.groupDelta.size();
		res.matchDeltaCount = res.matchDelta.size();

		if (change) {
			boolean updated = false;
			log.info("updating " + res.matchDelta.size() + " matches");
			for (MatchDTO[] g : res.matchDelta) {
				log.debug(g[0] + "!=" + g[1]);
				updated = true;
				try {
					if (g[1] != null) {
						if (g[0] != null) {
							matchService.updateMatch(g[1]);
						} else {
							matchService.addMatch(g[1]);
						}
					}
				} catch (EntityNotFoundException e) {
					log.warn("Failed " + e);
				}
			}

			log.info("updating " + res.groupDelta.size() + " groups");
			for (GroupDTO[] g : res.groupDelta) {
				log.debug(g[0] + "!=" + g[1]);
				updated = true;
				try {
					if (g[1] != null) {
						if (g[0] != null) {
							groupService.updateGroup(g[1]);
						} else {
							groupService.addGroup(g[1]);
						}
					}
				} catch (EntityNotFoundException e) {
					log.warn("Failed " + e);
				}
			}

			if (updated) {
				Util.getCache().clear();
				new SendMail().send("SyncOpenLigaDB",
						"<p>fixed in league " + l1 + ":</p><pre>"
								+ gson.toJson(res) + "</pre>",
						"lui", "lui.baeumer@gmail.com", null);
			}
		}

		return res;
	}

	static List<GroupDTO[]> delta(List<GroupDTO> groups1,
			List<GroupDTO> groups2) {
		if (groups1.size() != groups2.size()) {
			log.info("unequal group size " + groups1.size() + "!="
					+ groups2.size());
		}

		List<GroupDTO[]> res = new ArrayList<GroupDTO[]>();

		int i;
		for (i = 0; i < groups1.size(); i++) {
			GroupDTO g = groups1.get(i);

			GroupDTO a = null;
			for (GroupDTO tg : groups2) {
				if (tg.groupId == g.groupId) {
					a = tg;
					break;
				}
			}

			if (a == null) {
				// did not find a group, this group is new
				res.add(new GroupDTO[]{g, null});
			}
		}

		for (i = 0; i < groups2.size(); i++) {
			GroupDTO g = groups2.get(i);

			GroupDTO a = null;
			for (GroupDTO tg : groups1) {
				if (tg.groupId == g.groupId) {
					a = tg;
					break;
				}
			}

			if (a != null) {
				// found a corresponding group
				if (!equals(a, g)) {
					res.add(new GroupDTO[]{a, g});
				}
			} else {
				// did not find a group, this group is new
				res.add(new GroupDTO[]{null, g});
			}
		}

		return res;
	}

	static List<MatchDTO[]> deltaM(List<MatchDTO> matches1,
			List<MatchDTO> matches2) {

		if (matches1.size() != matches2.size()) {
			log.info("unequal match size " + matches1.size() + "!="
					+ matches2.size());
		}

		final List<MatchDTO[]> res = new ArrayList<MatchDTO[]>();

		int i;
		for (i = 0; i < matches1.size(); i++) {
			MatchDTO g = matches1.get(i);

			MatchDTO a = null;
			for (MatchDTO tg : matches2) {
				if (tg.matchId == g.matchId) {
					a = tg;
					break;
				}
			}

			if (a == null) {
				// did not find a match, this match is new
				res.add(new MatchDTO[]{g, null});
			}
		}

		for (i = 0; i < matches2.size(); i++) {
			MatchDTO g = matches2.get(i);

			MatchDTO a = null;
			for (MatchDTO tg : matches1) {
				if (tg.matchId == g.matchId) {
					a = tg;
					break;
				}
			}

			if (a != null) {
				// found a corresponding group
				log.debug("found a match");
				if (!equals(a, g)) {
					res.add(new MatchDTO[]{a, g});
				}
			} else {
				// did not find a match, this group is new
				log.info("found new match " + g);
				res.add(new MatchDTO[]{null, g});
			}
		}

		return res;
	}

	static boolean equals(GroupDTO g1, GroupDTO g2) {
		if (g1.groupId != g2.groupId)
			throw new IllegalStateException(g1.groupId + "!=" + g2.groupId);

		boolean r = (equals(g1.name, g2.name) && equals(g1.season, g2.season)
				&& equals(g1.groupId, g2.groupId)
				&& equals(g1.groupOrderId, g2.groupOrderId)
				&& equals(g1.firstMatchdate, g2.firstMatchdate)
				&& equals(g1.lastMatchdate, g2.lastMatchdate));
		return r;
	}

	static boolean equals(MatchDTO g1, MatchDTO g2) {
		if (g1.matchId != g2.matchId) {
			throw new IllegalStateException(g1.matchId + "!=" + g2.matchId);
		}

		if (!equals(g1.score1, g2.score1) || !equals(g1.score2, g2.score2)) {
			log.info("unequal score " + g1 + " vs " + g2);
		}
		return (equals(g1.team1.teamId, g2.team1.teamId)
				&& equals(g1.team2.teamId, g2.team2.teamId)
				&& equals(g1.season, g2.season)
				&& g1.matchdate.equals(g2.matchdate));
	}

	static boolean equals(String s1, String s2) {
		return ((s1 == null && s2 == null) || (s1 != null && s1.equals(s2)));
	}

	static boolean equals(Date s1, Date s2) {
		return ((s1 == null && s2 == null) || (s1 != null && s1.equals(s2)));
	}

	static boolean equals(int s1, int s2) {
		return s1 == s2;
	}

	static boolean equals(Integer s1, Integer s2) {
		return ((s1 == null && s2 == null) || (s1 != null && s1.equals(s2)));
	}
}
