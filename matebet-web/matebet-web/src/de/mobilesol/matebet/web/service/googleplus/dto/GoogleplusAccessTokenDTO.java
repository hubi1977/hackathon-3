package de.mobilesol.matebet.web.service.googleplus.dto;

public class GoogleplusAccessTokenDTO {
	public String access_token, token_type;
	public long expires_in;

	// public String id_token;

	@Override
	public String toString() {
		return "AccessTokenGooglePlus [access_token=" + access_token
				+ ", token_type=" + token_type + ", expires_in=" + expires_in
				+ "]";
	}
}
