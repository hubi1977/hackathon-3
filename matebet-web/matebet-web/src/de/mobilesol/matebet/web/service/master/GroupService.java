package de.mobilesol.matebet.web.service.master;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.cache.Cache;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.EntityNotFoundException;

import de.mobilesol.matebet.web.dao.GroupDAO;
import de.mobilesol.matebet.web.dto.GroupDTO;
import de.mobilesol.matebet.web.util.Util;

public class GroupService {
	private static final Logger log = Logger
			.getLogger(GroupService.class.getName());
	private static final int TIME_SHOW_GROUP_BEFORE_LAST_MATCH_END = 120;

	private GroupDAO dao = new GroupDAO();

	public int addGroup(GroupDTO a) {
		log.info("addGroup" + a);

		Util.getCache().remove("mb:group:league:" + a.leagueId);

		return dao.addGroup(a);
	}

	public void updateGroup(GroupDTO a) throws EntityNotFoundException {
		log.info("updateGroup" + a);

		Util.getCache().remove("mb:group:" + a.groupId);
		Util.getCache().remove("mb:group:league:" + a.leagueId);

		dao.updateGroup(a);
	}

	public GroupDTO getGroup(int id) {
		log.info("getGroup(" + id + ")");

		GroupDTO group;

		// read group from cache
		final String key = "mb:group:" + id;
		Cache c = null;
		try {
			c = Util.getCache();
			group = (GroupDTO) c.get(key);
			if (group == null) {
				log.info("reading from db");
				group = dao.getGroup(id);
				log.info("group=" + group);

				c.put(key, group);
			} else {
				log.info("reading from cache");
			}
		} catch (Exception e) {
			log.error("failed", e);
			group = dao.getGroup(id);
		}

		return group;
	}

	public List<GroupDTO> getAllGroups(int leagueId, boolean onlyActive) {
		log.info("getAllGroups(" + leagueId + ", " + onlyActive + ")");

		List<GroupDTO> groups = null;

		// read groups from cache
		final String key = "mb:group:league:" + leagueId;
		Cache c = null;
		try {
			c = Util.getCache();
			groups = (List<GroupDTO>) c.get(key);
			if (groups == null) {
				log.info("reading from db");
				groups = dao.getAllGroups(leagueId);
				c.put(key, groups);
			} else {
				log.info("reading from cache");
			}
		} catch (Exception e) {
			log.error("failed", e);
			groups = dao.getAllGroups(leagueId);
		}

		return groups;
	}
}
