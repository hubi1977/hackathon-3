package de.mobilesol.matebet.web.service.master;

import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import javax.cache.Cache;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.EntityNotFoundException;

import de.mobilesol.matebet.web.dao.MatchDAO;
import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchByGroupMap;
import de.mobilesol.matebet.web.util.ComparatorUtil;
import de.mobilesol.matebet.web.util.Util;

public class MatchService {
	private static final Logger log = Logger
			.getLogger(MatchService.class.getName());

	/**
	 * Starting bets will not be possible 15min before match start.
	 */
	private static final int TIME_BET_BEFORE_MATCH = 5;

	private MatchDAO dao = new MatchDAO();

	private TeamService teamService = new TeamService();

	public int addMatch(MatchDTO a) {
		log.info("addMatch" + a);
		int r = dao.addMatch(a);

		try {
			String key = "mb:match:" + a.matchId;
			Util.getCache().remove(key);
			key = "mb:matchbygroup:" + a.groupId;
			Util.getCache().remove(key);
		} catch (Exception e) {
			log.error("failed" + e);
		}

		return r;
	}

	public void updateMatch(MatchDTO a) throws EntityNotFoundException {
		log.info("updateMatch" + a);

		dao.updateMatch(a);

		try {
			String key = "mb:match:" + a.matchId;
			Util.getCache().remove(key);
			key = "mb:matchbygroup:" + a.groupId;
			Util.getCache().remove(key);
		} catch (Exception e) {
			log.error("failed" + e);
		}
	}

	public MatchDTO getMatch(int matchId) {
		log.info("getMatch(" + matchId + ")");
		Calendar current = new GregorianCalendar();
		current.add(Calendar.MINUTE, TIME_BET_BEFORE_MATCH);

		MatchDTO match;
		try {
			final String key = "mb:match:" + matchId;
			Cache c = Util.getCache();
			match = (MatchDTO) c.get(key);
			if (match == null) {
				match = dao.getMatch(matchId);
				c.put(key, match);
			} else {
				log.info("read from cache");
			}
		} catch (Exception e) {
			log.error("failed" + e);
			match = dao.getMatch(matchId);
		}

		match.team1.name = teamService.getTeam(match.team1.teamId).name;
		match.team2.name = teamService.getTeam(match.team2.teamId).name;
		match.finished = match.matchdate.before(current.getTime());

		log.info("match=" + match);

		return match;
	}

	public List<MatchDTO> getMatchesByGroup(int groupId) {
		log.info("getMatches(" + groupId + ")");
		Calendar current = new GregorianCalendar();
		current.add(Calendar.MINUTE, TIME_BET_BEFORE_MATCH);

		List<MatchDTO> matches;
		matches = dao.getMatchesByGroup(groupId);

		Collections.sort(matches, ComparatorUtil.MATCH_COMPARATOR);

		return matches;
	}

	public List<MatchDTO> getMatchesByLeague(int leagueId) {
		log.info("getMatchesByLeague(" + leagueId + ")");
		Calendar current = new GregorianCalendar();
		current.add(Calendar.MINUTE, TIME_BET_BEFORE_MATCH);

		List<MatchDTO> matches = dao.getMatchesByLeague(leagueId);
		for (MatchDTO m : matches) {
			m.team1.name = teamService.getTeam(m.team1.teamId).name;
			m.team2.name = teamService.getTeam(m.team2.teamId).name;
			if (m.team1.name == null || m.team2.name == null) {
				log.warn("did not find a team with id " + m.team1.teamId + "/"
						+ m.team2.teamId + ": " + m.team1.name + "/"
						+ m.team2.name);
			}
			m.finished = m.matchdate.before(current.getTime());
		}

		Collections.sort(matches, ComparatorUtil.MATCH_COMPARATOR);

		return matches;
	}

	public List<MatchDTO> getLatestMatches(int days) {
		return dao.getLatestMatches(days);
	}

	public List<MatchDTO> getMatchesBeginningSoon() {
		return dao.getMatchesBeginningSoon();
	}

	public TipsByMatchByGroupMap getMatchTips(int leagueId) {
		log.info("getMatchTips(" + leagueId + ")");

		final String key = "mb:tips:" + leagueId;

		TipsByMatchByGroupMap o = null;
		try {
			Cache c = Util.getCache();
			o = (TipsByMatchByGroupMap) c.get(key);
			if (o == null) {
				log.info("reading from db");
				o = dao.getMatchTips(leagueId);
				c.put(key, o);
			} else {
				log.info("read from cache");
			}
		} catch (Exception e) {
			log.error("failed", e);
			o = dao.getMatchTips(leagueId);
		}

		return o;
	}

	public MatchDTO setMatchResult(MatchDTO result) {
		MatchDTO ret = dao.setMatchResult(result);
		try {
			Cache c = Util.getCache();

			String key = "mb:tips:" + result.leagueId;
			c.remove(key);

			key = "mb:match:" + result.matchId;
			c.remove(key);

			key = "mb:matchbygroup:" + result.groupId;
			c.remove(key);
		} catch (Exception e) {
			log.error("failed", e);
		}

		return ret;
	}
}
