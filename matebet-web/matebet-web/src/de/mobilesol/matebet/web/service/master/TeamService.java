package de.mobilesol.matebet.web.service.master;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.cache.Cache;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dao.TeamDAO;
import de.mobilesol.matebet.web.dto.TeamDTO;
import de.mobilesol.matebet.web.util.Util;

public class TeamService {
	private static final Logger log = Logger
			.getLogger(TeamService.class.getName());
	private final String key = "mb:teammap";
	private TeamDAO dao = new TeamDAO();

	public int addTeam(TeamDTO a) {
		log.info("addTeam" + a);
		Util.getCache().remove(key);
		return dao.addTeam(a);
	}

	public TeamDTO getTeam(int id) {
		log.debug("getTeam(" + id + ")");
		TeamDTO team = getTeamMap().get(id);

		log.debug("team=" + team);
		if (team == null) {
			log.warn("team " + id + " does not exist.");
		}

		return team;
	}

	public Collection<TeamDTO> getAllTeams() {
		log.info("getAllTeams()");
		Collection<TeamDTO> t = getTeamMap().values();
		return t;
	}

	private Map<Integer, TeamDTO> getTeamMap() {

		Map<Integer, TeamDTO> o = null;
		Cache c = null;
		try {
			c = Util.getCache();
			o = (Map<Integer, TeamDTO>) c.get(key);
			if (o == null) {
				log.info("reading from teammap");
				List<TeamDTO> teams = dao.getAllTeams();
				log.info("loaded " + teams.size() + " teams");
				Map<Integer, TeamDTO> teamMap = new LinkedHashMap<Integer, TeamDTO>();
				for (TeamDTO t : teams) {
					Util.addTeamUrls(t);
					teamMap.put(t.teamId, t);
				}
				o = teamMap;
				c.put(key, o);

				return o;
			}
		} catch (Exception e) {
			log.error("failed", e);
			List<TeamDTO> teams = dao.getAllTeams();
			log.info("loaded " + teams.size() + " teams");
			Map<Integer, TeamDTO> teamMap = new HashMap<Integer, TeamDTO>();
			for (TeamDTO t : teams) {
				Util.addTeamUrls(t);
				teamMap.put(t.teamId, t);
			}
			o = teamMap;
		}

		return o;
	}
}
