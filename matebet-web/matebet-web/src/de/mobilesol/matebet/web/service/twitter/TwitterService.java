package de.mobilesol.matebet.web.service.twitter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.twitter.joauth.Normalizer;
import com.twitter.joauth.OAuthParams;
import com.twitter.joauth.Signer;
import com.twitter.joauth.UrlCodec;

import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.service.UserService;
import de.mobilesol.matebet.web.service.twitter.dto.TwitterAccessTokenDTO;
import de.mobilesol.matebet.web.service.twitter.dto.TwitterFriendDTO;
import de.mobilesol.matebet.web.service.twitter.dto.TwitterUserDTO;
import de.mobilesol.matebet.ws.dto.UserProfileDTO;

public class TwitterService {

	private static final Logger log = Logger
			.getLogger(TwitterService.class.getName());

	private UserService userService = new UserService();

	public UserProfileDTO readProfile(TwitterAccessTokenDTO token) {

		UserProfileDTO profile = new UserProfileDTO();
		profile.id = "tw:" + token.user_id;
		profile.name = token.screen_name;

		URL url;
		try {

			long timestampSecs = generateTimestamp();
			String nonce = generateNonce();

			Normalizer normalizer = Normalizer.getStandardNormalizer();
			Signer signer = Signer.getStandardSigner();

			OAuthParams.OAuth1Params oAuth1Params = new OAuthParams.OAuth1Params(
					token.oauth_token, "utcz1ff45h5zyDYVYXbgX1f3a", nonce,
					timestampSecs, Long.toString(timestampSecs), "",
					OAuthParams.HMAC_SHA1, OAuthParams.ONE_DOT_OH);

			List<com.twitter.joauth.Request.Pair> javaParams = new ArrayList<>();
			javaParams.add(new com.twitter.joauth.Request.Pair(
					UrlCodec.encode("user_id"), "" + token.user_id));
			javaParams.add(new com.twitter.joauth.Request.Pair(
					UrlCodec.encode("screen_name"), profile.name));
			javaParams.add(new com.twitter.joauth.Request.Pair(
					UrlCodec.encode("include_entities"), "false"));

			// https://api.twitter.com/1.1/friends/list.json?include_user_entities=true
			String normalized = normalizer.normalize("https", "api.twitter.com",
					443, "GET", "/1.1/users/show.json", javaParams,
					oAuth1Params);

			log.debug("normalised " + normalized);
			log.debug("secret " + token.oauth_token_secret);
			log.debug("consumerSecret "
					+ "OVbdGFEpNI2RMLGzlROJRMNWQ7xGAP7TXUpSfKK6PcHyLunQ9M");

			String signature = signer.getString(normalized,
					token.oauth_token_secret,
					"OVbdGFEpNI2RMLGzlROJRMNWQ7xGAP7TXUpSfKK6PcHyLunQ9M");

			Map<String, String> oauthHeaders = new LinkedHashMap<>();
			oauthHeaders.put(OAuthParams.OAUTH_CONSUMER_KEY,
					quoted("utcz1ff45h5zyDYVYXbgX1f3a"));
			oauthHeaders.put(OAuthParams.OAUTH_NONCE, quoted(nonce));
			oauthHeaders.put(OAuthParams.OAUTH_SIGNATURE, quoted(signature));
			oauthHeaders.put(OAuthParams.OAUTH_SIGNATURE_METHOD,
					quoted(OAuthParams.HMAC_SHA1));
			oauthHeaders.put(OAuthParams.OAUTH_TIMESTAMP,
					quoted(Long.toString(timestampSecs)));
			oauthHeaders.put(OAuthParams.OAUTH_TOKEN,
					quoted(token.oauth_token));
			oauthHeaders.put(OAuthParams.OAUTH_VERSION,
					quoted(OAuthParams.ONE_DOT_OH));

			String oauth = "OAuth " + Joiner.on(", ").withKeyValueSeparator("=")
					.join(oauthHeaders);
			log.debug("oauth=" + oauth);
			url = new URL("https://api.twitter.com/1.1/users/show.json?user_id="
					+ token.user_id + "&screen_name=" + profile.name
					+ "&include_entities=false");
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729 Lui)");

			connection.setRequestProperty("Authorization", oauth);

			InputStream in = connection.getInputStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in, "UTF-8"));
			try {
				String line;
				StringBuffer strb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					strb.append(line + "\n");
				}

				// if (log.isDebugEnabled()) {
				log.info("limited response friends from twitter=" + strb);
				// }

				Gson gson = new Gson();
				TwitterUserDTO user = gson.fromJson(strb.toString(),
						TwitterUserDTO.class);
				profile.picture = user.profile_image_url;

			} finally {
				reader.close();
			}

		} catch (Exception e) {
			log.warn("failed", e);
		}

		return profile;
	}

	public List<UserDTO> getFriends(String accessToken,
			String accessTokenSecret) {
		URL url;
		List<UserDTO> userFriends = new ArrayList<UserDTO>();

		try {

			long timestampSecs = generateTimestamp();
			String nonce = generateNonce();

			Normalizer normalizer = Normalizer.getStandardNormalizer();
			Signer signer = Signer.getStandardSigner();

			OAuthParams.OAuth1Params oAuth1Params = new OAuthParams.OAuth1Params(
					accessToken, "utcz1ff45h5zyDYVYXbgX1f3a", nonce,
					timestampSecs, Long.toString(timestampSecs), "",
					OAuthParams.HMAC_SHA1, OAuthParams.ONE_DOT_OH);

			List<com.twitter.joauth.Request.Pair> javaParams = new ArrayList<>();
			javaParams.add(new com.twitter.joauth.Request.Pair(
					UrlCodec.encode("count"), UrlCodec.encode("100")));

			// https://api.twitter.com/1.1/friends/list.json?include_user_entities=true
			String normalized = normalizer.normalize("https", "api.twitter.com",
					443, "GET", "/1.1/friends/ids.json", javaParams,
					oAuth1Params);

			log.debug("normalised " + normalized);
			log.debug("secret " + accessTokenSecret);
			log.debug("consumerSecret "
					+ "OVbdGFEpNI2RMLGzlROJRMNWQ7xGAP7TXUpSfKK6PcHyLunQ9M");

			String signature = signer.getString(normalized, accessTokenSecret,
					"OVbdGFEpNI2RMLGzlROJRMNWQ7xGAP7TXUpSfKK6PcHyLunQ9M");

			Map<String, String> oauthHeaders = new LinkedHashMap<>();
			oauthHeaders.put(OAuthParams.OAUTH_CONSUMER_KEY,
					quoted("utcz1ff45h5zyDYVYXbgX1f3a"));
			oauthHeaders.put(OAuthParams.OAUTH_NONCE, quoted(nonce));
			oauthHeaders.put(OAuthParams.OAUTH_SIGNATURE, quoted(signature));
			oauthHeaders.put(OAuthParams.OAUTH_SIGNATURE_METHOD,
					quoted(OAuthParams.HMAC_SHA1));
			oauthHeaders.put(OAuthParams.OAUTH_TIMESTAMP,
					quoted(Long.toString(timestampSecs)));
			oauthHeaders.put(OAuthParams.OAUTH_TOKEN, quoted(accessToken));
			oauthHeaders.put(OAuthParams.OAUTH_VERSION,
					quoted(OAuthParams.ONE_DOT_OH));

			String oauth = "OAuth " + Joiner.on(", ").withKeyValueSeparator("=")
					.join(oauthHeaders);
			log.debug("oauth=" + oauth);
			url = new URL(
					"https://api.twitter.com/1.1/friends/ids.json?count=100");
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729 Lui)");

			connection.setRequestProperty("Authorization", oauth);

			InputStream in = connection.getInputStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in, "UTF-8"));
			try {
				String line;
				StringBuffer strb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					strb.append(line + "\n");
				}

				if (log.isDebugEnabled()) {
					log.info("limited response friends from facebook=" + strb);
				}

				Gson gson = new Gson();
				TwitterFriendDTO user = gson.fromJson(strb.toString(),
						TwitterFriendDTO.class);

				log.info("users=" + user);
				for (long id : user.ids) {
					UserDTO uf = userService.getUserByExtId("tw:" + id);
					if (uf != null) {
						userFriends.add(uf);
					}
				}
			} finally {
				reader.close();
			}

		} catch (Exception e) {
			log.error("failed", e);
		}

		return userFriends;
	}

	private String quoted(String str) {
		return "\"" + str + "\"";
	}

	public long generateTimestamp() {
		long timestamp = System.currentTimeMillis();
		return timestamp / 1000;
	}

	public String generateNonce() {
		return Long.toString(Math.abs(new Random().nextInt(1000)))
				+ System.currentTimeMillis();
	}
}
