package de.mobilesol.matebet.web.service.twitter.dto;

public class TwitterUserDTO {
	public String id;
	public String name;
	public String profile_image_url;

	@Override
	public String toString() {
		return "UserFb [id=" + id + ", name=" + name + ", profile_image_url="
				+ profile_image_url + "]";
	}
}
