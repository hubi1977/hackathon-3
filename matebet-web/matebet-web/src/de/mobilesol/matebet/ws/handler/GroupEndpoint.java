package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import de.mobilesol.matebet.web.dto.GroupDTO;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;
import de.mobilesol.matebet.web.service.master.GroupService;

public class GroupEndpoint {

	private static final long serialVersionUID = 1L;

	private GroupService groupService = new GroupService();

	public GroupDTO handleGet(int groupId, HttpServletResponse resp)
			throws IOException {
		GroupDTO u = groupService.getGroup(groupId);
		if (u == null) {
			throw new ResourceNotFoundException(300,
					"Group with id " + groupId + " not found");
		}
		return u;
	}

	public List<GroupDTO> handleGetAllByLeague(int leagueId, boolean onlyActive,
			HttpServletResponse resp) throws IOException {
		List<GroupDTO> l = groupService.getAllGroups(leagueId, onlyActive);
		return l;
	}
}
