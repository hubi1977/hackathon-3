var projectApp = angular.module('projectApp', ['ngRoute',
    'openligaControllers', 'leagueControllers', 'teamControllers', 'pushControllers'
])

.filter('displaydate', function() {
    return function(input) {
	var date = new Date(input);
        return date.toLocaleDateString() + " " + date.toLocaleTimeString();
    }
})

.filter('displayday', function() {
    return function(input) {
	var date = new Date(input);
        return date.toLocaleTimeString();
    }
})
;

projectApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/openliga', {
        templateUrl: 'partials/oldbleaguelist.html',
        controller: 'OldbLeagueCtrl'
    }).when('/openliga/groups/:leagueId', {
        templateUrl: 'partials/oldbleaguegroups.html',
        controller: 'OldbLeagueGroupCtrl'
    }).when('/openliga/matches/:leagueId/:groupId', {
        templateUrl: 'partials/oldbleaguematches.html',
        controller: 'OldbLeagueMatchesCtrl'
    }).when('/league', {
        templateUrl: 'partials/leaguelist.html',
        controller: 'LeagueCtrl'
    }).when('/league/groups/:leagueId', {
        templateUrl: 'partials/leaguegroups.html',
        controller: 'LeagueGroupCtrl'
    }).when('/league/matches/:leagueId/:groupId', {
        templateUrl: 'partials/leaguematches.html',
        controller: 'LeagueMatchesCtrl'
    }).when('/teams', {
        templateUrl: 'partials/teams.html',
        controller: 'TeamCtrl'
    }).when('/urls', {
        templateUrl: 'partials/urls.html'
    }).when('/push', {
        templateUrl: 'partials/push.html',
        controller: 'PushCtrl'
    }).otherwise({
        redirectTo: '/league'
    });
}]);