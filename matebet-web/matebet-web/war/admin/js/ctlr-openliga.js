var pControllers = angular.module('openligaControllers', []);

pControllers.controller('OldbLeagueCtrl', ['$scope', '$http',
   function($scope, $http, $routeParams) {

       $scope.progressloading = true;
       $http({
           url: '/rs/openligadb/leagues/list',
           method: 'GET'
           }).success(function(data) {
               $scope.progressloading = false;
               $scope.leagues = data;

           }).error(function(data, status, headers, config) {
               $scope.progressloading = false;
           });
       }
   ]);

pControllers.controller('OldbLeagueGroupCtrl', ['$scope', '$http', '$routeParams',
   function($scope, $http, $routeParams) {

       $scope.progressloading = true;
       $http({
           url: '/rs/openligadb/group/list/' + $routeParams.leagueId,
           method: 'GET'
           }).success(function(data) {
               $scope.progressloading = false;
               $scope.leagueId = $routeParams.leagueId;
               $scope.groups = data;

           }).error(function(data, status, headers, config) {
               $scope.progressloading = false;
           });
       
       
       $scope.import = function(leagueId) {
	   console.log('importing ' + leagueId);

       
           if (confirm("Liga " + leagueId + " wirklich importieren?") == true) {

               $scope.progressloading = true;
               $http({
                   method: 'POST',
                   url: '/rs/openligadb/import/' + leagueId
               }).success(
                   function(data, status, headers,
                       config) {

                       $scope.progressloading = false;
                       alert('now importing league asynchronously');
                   }).error(
                   function(data, status, headers,
                       config) {

                       $scope.progressloading = false;
                       alert('something wrong here: '
                	       + JSON.stringify(data)
                	       + ', ' + status);
                   });
           }
       }
   }
]);

pControllers.controller('OldbLeagueMatchesCtrl', ['$scope', '$http', '$routeParams',
    function($scope, $http, $routeParams) {

        $scope.progressloading = true;
        $http({
            url: '/rs/openligadb/match/list/' + $routeParams.leagueId
            + '/' + $routeParams.groupId,
            method: 'GET'
        }).success(function(data) {
            $scope.progressloading = false;
            $scope.matches = data;

        }).error(function(data, status, headers, config) {
            $scope.progressloading = false;
        });
    }
]);

